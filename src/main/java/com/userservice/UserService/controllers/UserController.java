package com.userservice.UserService.controllers;

import com.userservice.UserService.dto.OrderDTO;
import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;
    @GetMapping("/getAllSampleUsers")
    public List<UserDTO> getAllSampleUsers(){
        return Arrays.asList(
                new UserDTO("1", "Thushan", "24"),
                new UserDTO("2", "Nishani", "22")
        );
    }

    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final String id){
        return userService.getOrdersByUserId(id);
    }

}
