package com.userservice.UserService.services;
import com.userservice.UserService.dto.OrderDTO;
import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.order-url}")
    private String getOrderServiceOrderUrl;

    @Autowired
    private RestTemplateBuilder restTemplate;

    @Autowired
    private UserRepository repository;

    public List<OrderDTO> getOrdersByUserId(String id){
        List<OrderDTO> orders = null;
        try {
            orders = restTemplate.build().getForObject(
                    orderServiceBaseUrl.concat(getOrderServiceOrderUrl).concat("/"+ id),
                    List.class
            );
        }
        catch (Exception e){
            LOGGER.warn("Exception in UserService -> getOrdersByUserId()" + e);
        }

        return orders;
    }

    public List<UserDTO> getAllUsers() {
        List<UserDTO> users = null;
        users = repository.findAll()
                .stream()
                .map(userEntity -> new UserDTO(
                        Long.toString(userEntity.getId()),
                        userEntity.getName(),
                        userEntity.getAge()
                )).collect(Collectors.toList());

        return users;
    }
}